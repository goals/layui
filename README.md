<p align="center">
  <a href="https://layui.dev">
    <img src="https://unpkg.com/outeres@0.1.0/img/layui/logo-icon.png" width="81" alt="Layui">
  </a>
</p>
<h1 align="center">Layui</h1>
<p align="center">
  Classic modular front-end UI library
</p>

<p align="center">  
  <a href="https://www.npmjs.com/package/layui">
    <img src="https://img.shields.io/npm/v/layui" alt="Version">
  </a>
  <a href="https://www.npmjs.com/package/layui">
    <img src="https://img.shields.io/github/license/layui/layui" alt="License">
  </a>
  <a href="https://github.com/layui/layui/blob/master/dist/css/layui.css">
    <img src="https://img.badgesize.io/layui/layui/master/dist/css/layui.css?compression=brotli&label=CSS%20Brotli%20size" alt="CSS Brotli size">
  </a>
  <a href="https://github.com/layui/layui/blob/master/dist/layui.js">
    <img src="https://img.badgesize.io/layui/layui/master/dist/layui.js?compression=brotli&label=JS%20Brotli%20size" alt="JS Brotli size">
  </a>
</p>

---

## 1.项目简介
Layui 是一套开源免费的 Web UI 组件库，采用自身轻量级模块化规范，遵循原生态的 `HTML/CSS/JavaScript` 开发模式，极易上手，拿来即用。
源项目地址：[https://gitee.com/layui/layui](https://gitee.com/layui/layui)

## 2.项目调整
本项目是在 layui 的 2.8.17 版本做的一个 fork 分支，并做的一些适应性的小调整
### 2.1 icon
1.添加 assets、eraser 图标

2.修改 close-fill、close、ok-circle 的 unicode 编码

3.添加 iconPicker 图标选择器模块

### 2.2 table
table的状态码由 0 改为通用的 200